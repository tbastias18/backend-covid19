package com.apicovid.ejercicio.controladores;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apicovid.ejercicio.entidades.Noticia;
import com.apicovid.ejercicio.modelos.RespuestaModel;
import com.apicovid.ejercicio.servicios.NoticiaService;

@RestController
@RequestMapping("/api")
public class PrincipalController {
	
	private Log log = LogFactory.getLog(PrincipalController.class);
	
	@Autowired
	private NoticiaService noticiaService;

	@GetMapping("/buscar")
	public ResponseEntity<Object> buscar(Pageable paginable, @RequestParam(required = false) String titulo, @RequestParam(required = false) String fuente) throws IOException {        
		RespuestaModel respuesta = new RespuestaModel();
		
		try {
			Page<Noticia> noticias = noticiaService.buscar(titulo, fuente, paginable);
			
			if (noticias.getContent().size() < 1) {
				noticiaService.buscarJornalia(fuente);
				noticias = noticiaService.buscar(titulo, fuente, paginable);
			}			
			return new ResponseEntity<>(noticias.getContent(), HttpStatus.OK);
		} catch(Exception e) {
			log.error(e.getMessage());
			log.info(e);
			 
			respuesta.setEstado("ERROR");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.BAD_REQUEST);
		}
	}
	
}
