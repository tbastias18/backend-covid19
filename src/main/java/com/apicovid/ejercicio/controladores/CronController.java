package com.apicovid.ejercicio.controladores;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.apicovid.ejercicio.entidades.Noticia;
import com.apicovid.ejercicio.servicios.NoticiaService;

@Configuration
@EnableScheduling
public class CronController {
	
	@Autowired
	private NoticiaService noticiaService;

	private Log log = LogFactory.getLog(PrincipalController.class);
	
	@Scheduled(cron="0 12 * * * *")
    public void eliminarCaducadas() {
		List<Noticia> todas = noticiaService.buscarTodas();
		
		for (Noticia n : todas) {
			long diff = new Date().getTime() - n.getFechaCreacion().getTime();
		    long diferencia = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	    	String id = "";
		    if (diferencia >= 5) {
		    	id = n.getId();
		    	noticiaService.eliminar(n);
				log.info("Eliminando noticia - ID: " + id);
		    }
		}
	}
		
}
