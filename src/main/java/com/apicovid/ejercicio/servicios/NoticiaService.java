package com.apicovid.ejercicio.servicios;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.apicovid.ejercicio.entidades.Noticia;
import com.apicovid.ejercicio.repositorios.NoticiaRepository;

@Service
public class NoticiaService {
	
	@Autowired
	private NoticiaRepository noticiaRepository;

    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
    
	public Noticia guardar(Noticia n) {
		return noticiaRepository.save(n);
	}
	
	public List<Noticia> buscarTodas() {
		return noticiaRepository.findAll();
	}
	
	@SuppressWarnings("unchecked")
	public List<Noticia> buscarJornalia(String fuente) throws Exception {
		try {
			String url = "https://api.jornalia.net/api/v1/articles?search=coronavirus+covid&startDate=2021-01-01";
			
			if (fuente != null && !fuente.equals("")) {
				url += "&providers=" + fuente;
			}
			
	        URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setDoOutput(true);
			con.setDoInput(true);
			
			con.setRequestProperty("Authorization", "Bearer " + "f36f0dc2f3204a3c821130384e208604");
			
			con.connect();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
			    content.append(inputLine);
			}
			in.close();

			con.disconnect();
			
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(content.toString());
			
			List<JSONObject> list = (List<JSONObject>) jsonObject.get("articles");
			List<Noticia> noticias = new ArrayList<>();
			for (JSONObject object : list) {
				Noticia noticia = new Noticia();
				noticia.setTitulo(object.get("title").toString());
				noticia.setFuente(((HashMap<String, String>) object.get("provider")).get("name").toString());
				noticia.setCuerpo(object.get("description").toString());
				noticia.setFechaPublicacion(format.parse(object.get("publishedAt").toString())); 
				noticia.setFechaCreacion(new Date());
				guardar(noticia);
				
				noticias.add(noticia);
			}
			
			return noticias;
		} catch(Exception e) {
			throw e;
		}
	}

	public Page<Noticia> buscar(String titulo, String fuente, Pageable paginable) {
		return noticiaRepository.buscar(titulo, fuente, paginable);
	}

	public void eliminar(Noticia n) {
		noticiaRepository.delete(n);
	}

}
