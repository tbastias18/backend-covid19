package com.apicovid.ejercicio.modelos;

import lombok.Data;

@Data
public class RespuestaModel {

	private String mensaje;
	private String estado;
	
}
