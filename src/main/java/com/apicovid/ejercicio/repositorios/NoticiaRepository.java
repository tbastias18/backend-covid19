package com.apicovid.ejercicio.repositorios;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.apicovid.ejercicio.entidades.Noticia;

@Repository("noticiaRepository")
public interface NoticiaRepository extends JpaRepository<Noticia, String> {

	@Query("SELECT n FROM Noticia n WHERE n.fechaPublicacion <= NOW() AND (:titulo IS NULL OR n.titulo LIKE :titulo) AND (:fuente IS NULL OR n.fuente LIKE :fuente)")
	public Page<Noticia> buscar(@Param("titulo") String titulo, @Param("fuente") String fuente, Pageable pageable);

}
